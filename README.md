# vim-gcov-coverage


## About this plugin

This plugin (gocv-coverage) parses the GCC gcov coverage reports produced
by e.g. meson build system and marks lines that are reported as uncovered
with a symbol in the vim's sign column.

The plugin implements minimum everything but so far it has been enough to meet the author's needs.

![screenshot](screenshot.png)


## Requirements

Tested with neovim v0.4.2. Requires Python 3 support and pyinotify module.


## Usage

The indicators can be loaded manually or the gcov report can be monitored
and the indicators then automatically kept up to date.

See `:help gcov-coverage` for more information about the usage of this
plugin.

Install the plugin as one usually does.


## License

Apache 2.0. See the file LICENSE for more information.
