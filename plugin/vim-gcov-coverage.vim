if exists('g:gcov_coverage_loaded')
	finish
endif

let s:plugin_dir = expand('<sfile>:p:h:h')

python3 << EOF

import sys
import vim
import os

plugin_dir = vim.eval('s:plugin_dir')
sys.path.insert(0, os.path.join(plugin_dir, 'python'))

import gcov_coverage

EOF

command -nargs=? ShowCoverage python3 gcov_coverage.show_coverage(<args>),
command          HideCoverage python3 gcov_coverage.hide_coverage()

command MonitorCoverage       python3 gcov_coverage.monitor_coverage()
command NoMonitorCoverage     python3 gcov_coverage.stop_monitoring()
command RefreshCoverage       python3 gcov_coverage.just_show_coverage()

let g:gcov_coverage_loaded = 1
