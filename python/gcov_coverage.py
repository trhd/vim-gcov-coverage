#!/usr/bin/python3

import vim  # type: ignore
import typing
import os
import pyinotify  # type: ignore
import time

INITIALIZED: bool = False
PATH: str = ""
WATCHER = None
NOTIFIER = None


class Callback(pyinotify.ProcessEvent):

    def __init__(self):
        self.stamp = time.clock_gettime_ns(time.CLOCK_MONOTONIC)

    def process_default(self, event) -> None:
        now = time.clock_gettime_ns(time.CLOCK_MONOTONIC)
        if now - self.stamp < 100 * 1000 * 1000:
            return
        vim.async_call(just_show_coverage)
        self.stamp = now


class ParsingException(Exception):
    pass


def get_default_path() -> str:
    path = vim.eval('g:gcov_coverage_path')
    if not path:
        raise ParsingException("No path to the coverage report defined.")
    return path


def parse_coverage_line(line: str) -> typing.Dict[str, typing.List[str]]:
    cols = line.split()
    if len(cols) < 5:
        return {}
    src_file = os.path.abspath(cols[0])
    missing_lines = cols[-1].split(',')
    return {src_file: missing_lines}


def parse_file() -> typing.Dict[str, typing.List[str]]:
    uncovered_lines = {}
    prefix = None
    try:
        with open(PATH, 'r') as handle:
            header = 6
            for line in handle.readlines():
                if line[:11] == 'Directory: ':
                    prefix = line[11:].strip()
                if header:
                    header -= 1
                    continue
                if line[:10] == '----------':
                    break
                assert prefix
                uncovered_lines.update(
                        parse_coverage_line(prefix + '/' + line))
    except FileNotFoundError:
        print("No coverage report available in '%s'." % PATH)
    return uncovered_lines


def place_signs(line_map: typing.Dict[str, typing.List[str]]) -> None:
    hide_coverage()
    bufs: typing.List[str] = [x.name for x in vim.buffers]
    for key, vals in line_map.items():
        for val in vals:
            line_range = val.split('-')
            begin = int(line_range[0])
            end = int(line_range[1]) if len(line_range) > 1 else begin
            for line in range(begin, end + 1):
                if key not in bufs:
                    continue
                vim.eval('sign_place({id}, "{grp}", "{sign}", "{fl}", {opts})'
                         .format(id=0,
                                 grp='gcov_coverage_sign_group',
                                 sign='gcov_coverage_sign',
                                 fl=key,
                                 opts=str({'lnum': line})))


def initialize_plugin(path: typing.Optional[str]) -> bool:
    global PATH, INITIALIZED
    if INITIALIZED:
        return False
    vim.command("let g:gcov_coverage_path = expand(get(g:, \
                'gcov_coverage_path', 'b/meson-logs/coverage.txt'))")
    PATH = path if path else get_default_path()
    if not os.path.exists(PATH):
        print("No coverage report (or anything) found from '%s'." % PATH)
        return True
    vim.command("let g:gcov_coverage_indicator = \
                 get(g:, 'gcov_coverage_indicator', '∅')")
    vim.command('call sign_define("gcov_coverage_sign", \
                {"texthl": "WarningMsg", "text": g:gcov_coverage_indicator})')
    INITIALIZED = True
    return False


def just_show_coverage() -> None:
    uncovered_lines = parse_file()
    place_signs(uncovered_lines)


def show_coverage(path: str = None) -> None:
    try:
        if not initialize_plugin(path):
            just_show_coverage()
    except ParsingException as exc:
        print(exc)


def notifier_running() -> bool:
    if not NOTIFIER:
        return False
    return NOTIFIER.is_alive()


def start_notifier() -> None:
    global NOTIFIER, WATCHER
    assert not NOTIFIER
    WATCHER = pyinotify.WatchManager()
    WATCHER.add_watch(os.path.dirname(PATH), pyinotify.ALL_EVENTS)
    NOTIFIER = pyinotify.ThreadedNotifier(WATCHER,
                                          default_proc_fun=Callback())
    NOTIFIER.start()


def stop_notifier() -> None:
    global NOTIFIER, WATCHER
    assert NOTIFIER
    NOTIFIER.stop()
    NOTIFIER = None
    WATCHER = None


def monitor_coverage(path: str = None) -> None:
    try:
        if initialize_plugin(path):
            return
        if not notifier_running():
            start_notifier()
        just_show_coverage()
    except ParsingException as exc:
        print(exc)


def stop_monitoring() -> None:
    if not notifier_running():
        print('No coverage monitoring seems to be running.')
    else:
        stop_notifier()
    hide_coverage()


def hide_coverage() -> None:
    vim.eval('sign_unplace("gcov_coverage_sign_group")')
