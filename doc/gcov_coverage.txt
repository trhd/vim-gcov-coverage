*gcov_coverage.txt*                                            *gcov-coverage*


CONTENTS                                                   *gcov-coverate-toc*
==============================================================================

 1. Introduction                                |gcov-coverage-introduction|
 2. Commands                                    |gcov-coverage-commands|
 3. Information                                 |gcov-coverage-info|
 4. License                                     |gcov-coverage-license|
 

INTRODUCTION                                     *gcov-coverage-introduction*
=============================================================================

This plugin parses the test coverage reports from gcov and marks the lines
that are reported as uncovered with an indicator in the vim's sign column.

The indicators can be setup once or optionally whenever the coverage report is
updated.

The source code for the plugin can be found from

  https://gitlab.com/trhd/vim-gcov-coverage
  
and that is also the prefered way to provide (constructive) feedback, bug
reports etc.


COMMANDS                                              *gcov-coverage-commands*
==============================================================================

`:ShowCoverage [path_to_report]`                               *:ShowCoverage*

This command will parse the coverage report and places the indicators
accordingly to the sign column.

The path to the coverage reports is given as an argument or omitted in
which case the path is gread from g:gcov_marker_path.


:HideCoverage                                                  *:HideCoverage*

This command removes the indicators previously setup with the
|ShowCoverage| command.


:RefreshCoverage                                           *:RefreshCoverage*

This command reloads the coverage report and updates the indicators'
places.


:MonitorCoverage                                           *:MonitorCoverage*

This command will monitor the coverage report file for changes and
automatically update the indicators whenever the reports is updated.

Sort of like:

  |ShowCoverage| ; while (wait_for_update()) { |RefreshCoverage| }


:NoMonitorCoverage                                       *:NoMonitorCoverage*

This command will stop the plugin from monitoring any new updates from the
coverage reports and hide currently shown indicators.


CONFIGURATION                                      *gcov-marker-configuration*
==============================================================================

The following configuration variables can be used to customize the
plugin's behaviour:


`g:gcov_coverate_path`                                  *g:gcov_coverate_path*

This variable should point to the path where the coverage report can be
found. Makes sense to use a relative path here.

Defaults to 'b/meson-logs/coverage.txt'.


`g:gcov_coverate_indicator`                        *g:gcov_coverate_indicator*

This variable holds the character/sign that will be used to indicate lines
that have not been covered according to the parsed coverage reports.

Defaults to '∅'.


==============================================================================
LICENSE                                                *gcov-coverage-license*

Licensed under MIT license. See the file LICENSE for more details.


vim: ft=help
